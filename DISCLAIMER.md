Disclaimer
==========

These scripts are intended for research use; they are untested and maintained only through best efforts.
The corresponding Docker images are not intended for operation in secure environments.
The BICV group members and/or Imperial College London are not liable for loss, injury or other harm resulting from use of this code.
