dockerfiles
===========
A compilation of [Dockerfiles](https://docs.docker.com/reference/builder/) developed by members of BICV.
Dependencies are indicated left to right e.g. cuda-vnc is VNC built on top of CUDA.
Primary development may take place in other repositories. Please refer to them for the most up-to-date information:

- Kai Arulkumaran
    - [GitHub](https://github.com/Kaixhin/dockerfiles)
    - [Docker Hub](https://registry.hub.docker.com/repos/kaixhin/)
- Jose Rivera-Rubio
    - [GitHub](https://github.com/jmrr/dockerfiles)
    - [Docker Hub](https://registry.hub.docker.com/repos/josemrivera/)